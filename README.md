# Install Code Ready Containers on a Laptop

Installing Code Ready Containers, CRC,  will create a vm with an all-in-one Openshift deployment.  The default memory size is 8Gb, but can be modified to support larger containers like Ansible Tower. CRC can be installed on Windows, Mac and Linux.  These steps are tested on Fedora 31 with libvirt pre-installed, sudo enabled.  CRC will create a NATed network named crc and will enable dnsmasq on localhost to resolve crc.testing and apps-crc.testing to the crc vm's ip address.

CRC will require a pull secret.  Log into cloud.redhat.com to obtain a secret.  A redhat id can be created for free on redhat.com.  It may need to be actviated at developer.redhat.com first.

Download the pull secret at cloud.redhat.com

under Red Hat OpenShift Cluster Manager
* click Cluster Manager
* click Create Cluster
* click Laptop
* click Download pull secret


Download and install CRC and the oc client

    curl https://mirror.openshift.com/pub/openshift-v4/clients/crc/latest/crc-linux-amd64.tar.xz > crc.tar.gz
    tar xvf crc-linux.tar.gz 
    mkdir ~/bin
    mv crc-linux-<version>/crc ~/bin/

Install an openshift client
    curl https://mirror.openshift.com/pub/openshift-v4/clients/oc/4.2/linux/oc.tar.gz > oc.tar.gz
    tar xvf oc.tar.gz
    mv oc ~/bin/

Configure the pull secret file, and sufficient memory and cpu

    mkdir .crc
    mv ~/Downloads/pull-secret ~/.crc/
    crc config pull-secret-file ~/.crc/pull-secret
    crc config memory 16384
    crc config cpus 8

Setup and Start CRC
    crc setup
    crc start

The vm image and config is in ~/.crc/cache, along with the vm configuration and kubeadmin password

# References:

https://access.redhat.com/documentation/en-us/red_hat_codeready_containers/1.0/
https://developers.redhat.com/blog/2018/10/26/installing-and-managing-ansible-tower-on-red-hat-openshift-container-platform/
https://docs.ansible.com/ansible-tower/latest/html/administration/openshift_configuration.html



