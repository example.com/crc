## Installing Ansible Tower on Openshift

    wget  https://releases.ansible.com/ansible-tower/setup_openshift/ansible-tower-openshift-setup-latest.tar.gz
    tar xvf  ansible-tower-openshift-setup-latest.tar.gz
    cd ansible-tower-openshift-setup-latest

    oc login -u developer -p $(cat ~/.crc/cache/crc_libvirt_4.2.2/kubeadmin-password) https://api.crc.testing:6443

    oc new-project ansible-tower

The ansible tower installer expects a pvc named postgresql.  CRC has prebuilt 10Gi emphemeral pvs.

    cat postgresql_pvc.yml

    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: postgresql
    spec:
      accessModes:
      - ReadWriteOnce
      resources:
        requests:
          storage: 10Gi

    oc create -f postgresql_pvc.yml

    oc get pvc, verify it is bound


Edit the inventory file to include these variable settings

    openshift_host=https://api.crc.testing:6443
    openshift_project=ansible-tower
    openshift_user=kubeadmin
    openshift_password=
    admin_password=redhat
    pg_username=pguser
    pg_password=redhat
    rabbitmq_password=redhat
    rabbitmq_erlang_cookie=redhat

Run the installer, also used for backup and recovery
    ./setup_openshift.sh

If the install fails to login to openshift due to ssl issues edit roles/kubernetes/tasks/openshift_auth.yml
on line 21 change the line to --insecure-skip-tls-verify=true


This script uses the oc new-app command to deploy a postresql databases with a deployment config and, and a stateful set for the ansible-tower pod. The stafeful set can be scaled to add additional ansible-tower pods

    oc get po

    oc get routes


Git content replicated by Tower can be seen here
    oc exec -it -c ansible-tower-celery ansible-tower-0 ls /var/lib/awx/projects
